# Word o't' Day

[![pipeline status](https://gitlab.com/DannyEdwards/word-ot-day/badges/master/pipeline.svg)](https://gitlab.com/DannyEdwards/word-ot-day/-/commits/master)

## Environment

Copy `env.example.ts` to `env.ts` and populate with any environment variables you'd like to use with the project.

## Commands

To build and watch JS files.

```bash
npm start
```

To build the production bundle.

```bash
npm run build
```
