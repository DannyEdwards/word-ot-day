import { PROXY_URL } from "../../env.ts";

const WEBSTER_URL = "https://www.merriam-webster.com/wotd/feed/rss2";

export default async function getWords() {
  const response = await fetch(`${PROXY_URL}${WEBSTER_URL}`);
  if (!response.ok) throw "⚠️ Connection to Webster was unsuccessful.";
  const textXML = await response.text();
  const DOMFromString = new DOMParser().parseFromString(textXML, "text/xml");
  return [...DOMFromString.querySelectorAll("item")]
    .map((itemEl) => {
      const { heading, link, definition, pubDate } = getPropsFromItemEl(itemEl);
      if (!heading || !link || !definition || !pubDate)
        throw new SyntaxError("Malformed XML response.");
      return { heading, link, date: new Date(pubDate), definition };
    })
    .reverse();
}

function getPropsFromItemEl(element: Element) {
  return {
    heading: element.querySelector("title")?.textContent,
    link: element.querySelector("link")?.textContent,
    definition: element.children[10]?.textContent,
    pubDate: element.querySelector("pubDate")?.textContent,
  };
}
