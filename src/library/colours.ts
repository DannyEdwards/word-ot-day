function shuffle(array: any[]) {
  let index = array.length,
    random: number;
  while (index) {
    random = Math.floor(Math.random() * index--); // @note Pick a remaining element.
    [array[index], array[random]] = [array[random], array[index]]; // @note Swap.
  }
  return array;
}

export default shuffle([
  "tomato",
  "mediumseagreen",
  "steelblue",
  "indianred",
  "teal",
  "goldenrod",
  "mediumorchid",
  "peru",
  "cadetblue",
]);
