import {
  ERROR_MESSAGE,
  OFFLINE_MESSAGE,
} from "./components/notification-toaster.ts";
import { insertWord } from "./components/word-item.ts";
import { loaderEl, mainEl, notificationEl } from "./library/elements.ts";
import getWords from "./library/webster.ts";

async function handleLoad() {
  try {
    const words = await getWords();
    words.map(insertWord, mainEl);
    mainEl.scrollTo(mainEl.scrollWidth, 0);
  } catch (error) {
    notificationEl.show(typeof error === "string" ? error : ERROR_MESSAGE);
    console.error(error);
  } finally {
    loaderEl.remove();
  }
  if (!navigator.onLine) notificationEl.show(OFFLINE_MESSAGE);
  if ("serviceWorker" in navigator) navigator.serviceWorker.register("sw.js");
}

window.addEventListener("load", handleLoad);
