import colours from "../library/colours.ts";

const ACTIVE_CLASS = "active";

export interface Attributes {
  heading: string;
  definition: string;
  date: Date;
  link: string;
  specialDay?: string;
}

export default class WordItem extends HTMLElement {
  heading: string;
  definition: string;
  date: Date;
  link: string;
  specialDay?: string;
  constructor({ heading, definition, date, link, specialDay }: Attributes) {
    super();
    this.heading = heading;
    this.definition = definition;
    this.date = date;
    this.link = link;
    this.specialDay = specialDay;
  }
  connectedCallback() {
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.innerHTML = this.render();
  }
  addIntersectionClass(className: string) {
    new IntersectionObserver((entries) =>
      entries.forEach(({ target, isIntersecting }) =>
        target.classList.toggle(className, isIntersecting),
      ),
    ).observe(this);
  }
  protected render() {
    return /*html*/ `
      <style>
        @keyframes fadein {
          from { opacity: 0; }
          to { opacity: 1; }
        }
        :host {
          animation: fadein 2s;
          box-sizing: border-box;
          /* This slight overlap is to ensure only one panel is active */
          flex: 0 0 101%;
          height: 100vh;
          justify-content: center;
          padding: 1em;
          scroll-snap-align: center;
        }
        :host > * {
          max-width: 400px;
          margin-left: auto;
          margin-right: auto;
        }
        :host > *:last-child {
          margin-bottom: 0;
        }
        :host(.${ACTIVE_CLASS}) {
          height: auto;
        }
        h1 {
          hyphens: auto;
          word-break: break-word;
        }
        a {
          color: white;
          text-decoration: none;
        }
        a:hover {
          text-decoration: underline;
        }
        p {
          font-family: serif;
        }
        time,
        i {
          opacity: 0.5;
          font-size: 0.75em;
        }
        time {
          display: block;
          text-align: center;
        }
      </style>
      <time datetime="${this.date.toString()}">${this.date.toDateString()}</time>
      <h1>
        <a href="${this.link}" target="_blank" rel="noopener">
          ${this.heading}
        </a>
      </h1>
      ${this.specialDay ? /*html*/ `<p><i>${this.specialDay}</i></p>` : ""}
      <p>${this.definition}</p>
    `;
  }
}

customElements.define("word-item", WordItem);

export function insertWord(
  this: HTMLElement,
  { date, heading, definition, link, specialDay }: Attributes,
  index: number,
) {
  const wordItemEl = new WordItem({
    heading,
    definition,
    date,
    link,
    specialDay,
  });
  wordItemEl.classList.add(colours[index]);
  wordItemEl.addIntersectionClass(ACTIVE_CLASS);
  this.append(wordItemEl);
}
